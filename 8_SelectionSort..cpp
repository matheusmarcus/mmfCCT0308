#include <iostream>

using namespace std;

const int tamanho = 20;

int vetor[tamanho];

void lerVetor(int vet[], int tam){
    for(int i =0; i < tam; i++){
        cout << "Posicao " << (i+1) << ": ";
        cin >> vet[i];
    }
}

void imprimirVetor(int vet[], int tam){
     cout << "A posicao do vetor ordenado pelo selection sort e:" << endl;

    for(int i =0; i < tam; i++){
        cout << "Posicao " << (i+1) << ": " << vet[i] << endl;
    }
}

void selectionSort(int vet[], int tam){

    for(int i = 0; i < tam; i++){
        int menor = i;
        for(int j =i+1; j < tam; j++){
            if(vet[j] < vet[menor]){
                menor = j;
            }
        }
        if (i != menor){
            int temp = vet[i];
            vet[i] = vet[menor];
            vet[menor] = temp;
        }
    }
}

int main()
{
    lerVetor(vetor, tamanho);
    selectionSort(vetor,tamanho);
    imprimirVetor(vetor,tamanho);
}
